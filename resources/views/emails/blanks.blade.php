<table width="433" border="1">
  <tr bgcolor="#99CC33">
    <td colspan="2"><div align="center">Chi tiết hóa đơn </div></td>
  </tr>
  @foreach($cart as $item)
  <tr>
    <td width="83"><div align="right"><span class="style1"><a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}">{{$item->name}}</a></span></div></td>
    <td width="334"><div align="center"><span class="style2">{{$item->price}}</span> VND </div></td>
  </tr>
  @endforeach
  <tr bgcolor="#99CC33">
    <td colspan="2" bgcolor="#FFFFFF"><div align="right">Tổng cộng: {{Cart::total()}} VND </div></td>
  </tr>
  <tr bgcolor="#99CC33">
    <td colspan="2"><div align="center">Cảm ơn quí khách</div></td>
  </tr>
