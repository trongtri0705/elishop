<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>SimpleOne - A Responsive Html5 Ecommerce Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="@yield('description')">
        <meta name="author" content="">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
        <link href="{!! asset('public/simpleone/css/bootstrap.css')!!}" rel="stylesheet">
        <link href="{!! asset('public/simpleone/css/bootstrap-responsive.css')!!}" rel="stylesheet">
        <link href="{!! asset('public/simpleone/css/style.css')!!}" rel="stylesheet">
        <link href="{!! asset('public/simpleone/css/flexslider.css')!!}" type="text/css" media="screen" rel="stylesheet"  />
        <link href="{!! asset('public/simpleone/css/jquery.fancybox.css')!!}" rel="stylesheet">
        <link href="{!! asset('public/simpleone/css/cloud-zoom.css')!!}" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js')!!}"></script>
            <![endif]-->
        <!-- fav -->
        <link rel="shortcut icon" href="{!! asset('public/sanpham/Apple-03.png')!!}">
    </head>
    <body id="body">
        <!-- Header Start -->
        @include('simpleone.header')
        <!-- Header End -->
        @yield('content')
        <!-- Footer -->
        @include('simpleone.footer')
        <!-- javascript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="{!! asset('public/simpleone/js/jquery.js')!!}"></script>
        <script src="{!! asset('public/simpleone/js/bootstrap.js')!!}"></script>
        <script src="{!! asset('public/simpleone/js/jquery.pjax.js')!!}"></script>
        <script src="{!! asset('public/simpleone/js/respond.min.js')!!}"></script>
        <script src="{!! asset('public/simpleone/js/application.js')!!}"></script>
        <script src="{!! asset('public/simpleone/js/bootstrap-tooltip.js')!!}"></script>
        <script defer src="{!! asset('public/simpleone/js/jquery.fancybox.js')!!}"></script>
        <script defer src="{!! asset('public/simpleone/js/jquery.flexslider.js')!!}"></script>
        <script type="text/javascript" src="{!! asset('public/simpleone/js/jquery.tweet.js')!!}"></script>
        <script  src="{!! asset('public/simpleone/js/cloud-zoom.1.0.2.js')!!}"></script>
        <script  type="text/javascript" src="{!! asset('public/simpleone/js/jquery.validate.js')!!}"></script>
        <script type="text/javascript"  src="{!! asset('public/simpleone/js/jquery.carouFredSel-6.1.0-packed.js')!!}"></script>
        <script type="text/javascript"  src="{!! asset('public/simpleone/js/jquery.mousewheel.min.js')!!}"></script>
        <script type="text/javascript"  src="{!! asset('public/simpleone/js/jquery.touchSwipe.min.js')!!}"></script>
        <script type="text/javascript"  src="{!! asset('public/simpleone/js/jquery.ba-throttle-debounce.min.js')!!}"></script>
        <script defer src="{!! asset('public/simpleone/js/custom.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/simpleone/js/myscript.js')!!}"></script>
        <script type="text/javascript">
                    $(function(){
                    // pjax
                    $(document).pjax('nav a', '#body')
                    })
                    $(document).ready(function(){
            // does current browser support PJAX
            if ($.support.pjax) {
            $.pjax.defaults.timeout = 2000; // time in milliseconds
            }
            });
        </script>
    </body>
</html>