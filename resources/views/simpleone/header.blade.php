
<header>
    <div class="headerstrip">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <a href="{!!url('/')!!}" class="logo pull-left"><img src="{!!url('public/simpleone/img/logo.png')!!}" alt="SimpleOne" title="SimpleOne"></a>
                    <!-- Top Nav Start -->
                    <div class="pull-left">
                        <div class="navbar" id="topnav">
                            <div class="navbar-inner">
                                <ul class="nav" >
                                    <li><a class="home active" href="{!!url('/')!!}">Home</a>
                                    </li>
                                    <li><a class="myaccount" href="#">My Account</a>
                                    </li>
                                    <li><a class="shoppingcart" href="{!!url('gio-hang')!!}">Shopping Cart</a>
                                    </li>
                                    <li><a class="checkout" href="{!!url('check-out')!!}">CheckOut</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav End -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="categorymenu">
            <nav class="subnav">
                <ul class="nav-pills categorymenu">
                    <?php
                    $cate = DB::table('eli_department')->select('*')->where('active', 1)->get();
                    ?>
                    @foreach($cate as $parent)
                    <li><a href="javascript:void(0)">{!!$parent->name!!}</a>
                        <div>
                            <ul>  
                                <?php
                                $cate_child = DB::table('eli_category')->select('*')->where(['id_department' => $parent->id, 'active' => 1])->get();
                                ?>              
                                @foreach( $cate_child as $child)

                                <li><a href="{!!url('loai-san-pham',[$child->id,changeTitle(strtolower($child->name))])!!}">{!!$child->name!!} </a></li>
                                @endforeach

                            </ul>              
                        </div></li>
                    @endforeach
                    <li><a href="{!!url('contact')!!}">Contact US</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>