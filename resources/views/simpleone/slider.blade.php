<section class="slider">
    <div class="container">
      <div class="flexslider" id="mainslider">
        <ul class="slides">
          <?php
           $image = DB::table('eli_product_image')->select('*')->where('id_product',0)->get();
          ?>
          @foreach($image as $item)
          <li>
            <img src="{!!asset('public/sanpham/'.$item->image)!!}" alt="" />
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </section>