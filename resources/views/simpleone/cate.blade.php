@extends('simpleone.master')
@section('content')
<style type="text/css">
    .breadcrumb .pull-right{
        text-transform: uppercase;
        font-weight: bold;
        color: rgba(0,0,0,0.8);
    }
</style>
<div id="maincontainer">
    <section id="product">
        <div class="container">
            <!--  breadcrumb -->  
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                    <span class="divider">/</span>
                </li>
                <li class="active">Category</li>
                <li class="pull-right">
                    {!!$menu_cate[0]->name!!}
                </li>
            </ul>
            <div class="pull-right">
                <p id="type-view" style="cursor:pointer">LIST</p>
            </div>
            <div class="row">        
                <!-- Sidebar Start-->
                <aside class="span3">
                    <!-- Category-->  
                    <div class="sidewidt">
                        <h2 class="heading2"><span>Categories</span></h2>
                        <ul class="nav nav-list categories">
                            @foreach($menu_cate as $item)
                            <li>
                                <a href="{!!url('loai-san-pham',[$item->id,changeTitle($item->name)])!!}">{!!$item->name!!}</a>
                            </li>  
                            @endforeach           
                        </ul>
                    </div>
                    <!--  Best Seller -->  
                    <div class="sidewidt">
                        <h2 class="heading2"><span>Best Seller</span></h2>
                        <marquee height="300"  behavior="scroll" direction="up" scrolldelay="100" scrollamount="3" onMouseOver="this.stop();" onMouseOut="this.start();">
                            <ul class="bestseller" style="margin-top:-300px">
                                <!-- foreach here -->                            
                                @foreach($bestseller as $item)
                                <li>
                                    <img width="50" height="50" src="{!!asset('public/sanpham/'.getImage($item->id))!!}" alt="" title="">
                                    <a class="productname" href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}"> {!!$item->name!!}</a>
                                    <span class="procategory"><?php
                                        $parent = DB::table('eli_category')->select('*')->where('id', $item->id_category)->first();
                                        echo $parent->name;
                                        ?></span>
                                    <span class="price">{!!$item->price!!}</span>
                                </li>
                                @endforeach
                            </ul>
                        </marquee>
                    </div>
                    <!-- Latest Product -->  
                    <div class="sidewidt">
                        <h2 class="heading2"><span>Latest Products</span></h2>
                        <marquee height="300" behavior="scroll" direction="up" scrolldelay="100" scrollamount="3" onMouseOver="this.stop();" onMouseOut="this.start();">
                            <ul class="bestseller" style="margin-top:-300px">
                                <!-- foreach here -->
                                @foreach($latest as $item)
                                <li>
                                    <img width="50" height="50" src="{!!asset('public/sanpham/'.getImage($item->id))!!}" alt="" title="">
                                    <a class="productname" href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}"> {!!$item->name!!}</a>
                                    <span class="procategory">
                                        <?php
                                        $parent = DB::table('eli_category')->select('*')->where('id', $item->id_category)->first();
                                        echo $parent->name;
                                        ?>

                                    </span>
                                    <span class="price">{!!$item->price!!}</span>
                                </li>
                                @endforeach
                            </ul>
                        </marquee>
                    </div>
                    <!--  Must have -->  
                    <div class="sidewidt">
                        <h2 class="heading2"><span>Must have</span></h2>
                        <div class="flexslider" id="mainslider">
                            <ul class="slides">
                                <li>
                                    <img src="img/product1.jpg" alt="" />
                                </li>
                                <li>
                                    <img src="img/product2.jpg" alt="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>
                <!-- Sidebar End-->
                <!-- Category-->
                <div class="span9">          
                    <!-- Category Products-->
                    <section id="category">
                        <div class="row">
                            <div class="span9">
                                <!-- Category-->
                                <section id="categorygrid">
                                    @if(count($product_cate)>0)
                                    <ul class="thumbnails grid">
                                        <!-- foreach here -->

                                        @foreach($product_cate as $item)
                                        <li class="span3">
                                            <a class="prdocutname" href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}">{!!$item->name!!}</a>
                                            <div class="thumbnail span3">

                                                <a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}"><img alt="" src="{!!asset('public/sanpham/'.getImage($item->id))!!}"></a>
                                                <div class="pricetag">
                                                    <span class="spiral"></span><a href="{!!url('mua-hang',[$item->id,changeTitle($item->name)])!!}" class="productcart">ADD TO CART</a>
                                                    <div class="price">
                                                        <div class="pricenew">{!!number_format($item->price,0,'.',',')!!}</div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="display inactive">{!!$item->description!!}</div>
                                        </li>

                                        @endforeach

                                    </ul>

                                    Tong so trang{!!$product_cate->lastPage()!!}
                                    <div class="pagination pull-right">
                                        <ul>
                                            @if($product_cate->currentPage()!=1)
                                            <li><a href="{!!str_replace('/?','?',$product_cate->url(1))!!}">First</a></li>
                                            <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()-1))!!}">Prev</a></li>
                                            @endif
                                            @for($i=1;$i<=$product_cate->lastPage();$i++)
                                            @if($i>$product_cate->currentPage()-5 && $i<$product_cate->currentPage()+5)
                                            <li class="{!!($product_cate->currentPage()==$i) ? 'active':''!!}"><a href="{!!str_replace('/?','?',$product_cate->url($i))!!}">{!!$i!!}</a></li>                      
                                            @endif
                                            @endfor
                                            @if($product_cate->currentPage()!=$product_cate->lastPage())
                                            <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()+1))!!}">Next</a></li>
                                            <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->lastPage()))!!}">Last</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                    @else
                                    <h2>Products are gonna be updated...</h2>
                                    @endif
                                </section>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection