<section id="latest" class="row">
    <div class="container">
        <h1 class="heading1"><span class="maintext">Latest Products</span><span class="subtext"> See Our  Latest Products</span></h1>
        <ul class="thumbnails">
            @foreach($latest as $item)
            <li class="span4">
                <a class="prdocutname" href="{!!url('chi-tiet-san-pham',[$item->id,$item->id])!!}">{!!$item->name!!}</a>
                <div class="thumbnail">
                    <?php
                    $image = DB::table('eli_product_image')->select('*')->where('id_product', $item->id)->first();
                    ?>
                    <a href="{!!url('chi-tiet-san-pham',[$item->id,$item->id])!!}"><img alt="" src="{!!asset('public/sanpham/'.$image->image)!!}"></a>
                    <div class="pricetag">
                        <span class="spiral"></span><a href="{!!url('mua-hang',[$item->id,changeTitle(strtolower($item->name))])!!}" class="productcart">ADD TO CART</a>
                        <div class="price">
                            <div class="pricenew">{!!number_format($item->price,0,'.',',')!!}</div>

                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</section>