@extends('Estore16.master')
@section('content')
    <div class="content_sec">
        <div class="col3">
            <div class="col3_top">&nbsp;</div>
            <div class="col3_center">
                <h2 class="heading colr">Login</h2>
                <div class="login">
                	<div class="registrd">
                    	<h3>Please Log In</h3>
                        <p>If you have an account with us, please log in.</p>
                        <form class="form-horizontal" role="form" method="POST" action="{!!url('auth/login')!!}">
                        <input type="hidden" name="_token" value="{!! csrf_token()!!}" />
                         @include('admin.block.error') 
                         <h2>{{Session::get('msg')}}</h2>
                        <ul class="forms">
                        	<li class="txt">Email Address <span class="req">*</span></li>
                            <li class="inputfield"><input class="form-control" placeholder="E-mail" name="email" type="email" autofocus></li>
                        </ul>
                        <ul class="forms">
                        	<li class="txt">Password <span class="req">*</span></li>
                            <li class="inputfield"><input class="form-control" placeholder="Password" name="password" type="password" value=""></li>
                        </ul>
                        <ul class="forms">
                        	<li class="txt">&nbsp;</li>
                            <li><button class="simplebtn" type="submit" class="btn btn-lg btn-success btn-block"><span>Login</span></button> <a href="{!!url('authentication/getedit')!!}"" class="forgot">Forgot Your Password?</a></li>
                        </ul>
                        
                        </form>                        
                    </div>
                    <div class="newcus">
                    	<h3>Please Sign In</h3>
                        <p>
                        	By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.
                        </p>
                        <a href="{!!url('authentication/getregister')!!}" class="simplebtn"><span>Register</span></a>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="col3_botm">&nbsp;</div>
        </div>
    </div>
    <div class="clear"></div>
@endsection