@extends('Estore16.master')
@section('content')

    <div class="content_sec">
    	<!-- Column2 Section -->
        <div class="col2">
        	<div class="col2_top">&nbsp;</div>
            <div class="col2_center">
        	<h4 class="heading colr">My Account</h4>
            <div class="account">
            	<ul class="acount_navi">
                    <li><a  class="selected">Account Home</a></li>
                    <li><a >Order History</a></li>
                    <li><a >My Profile</a></li>
                    <li><a >Support</a></li>
                    <li><a >Wishlist</a></li>
                    <li><a >Logout</a></li>
                </ul>
                <div class="account_title">
                    <h6 class="bold">Hello, John!</h6>
                    <p>
                        From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.
                    </p>
                </div>
                <div class="clear"></div>
                <div class="acount_info">
                    <h6 class="heading bold colr">Account Information</h6>
                    <div class="big_sec left">
                        <div class="big_small_sec left">
                        	<div class="headng">
                                <h6 class="bold">Contact Information</h6>
                                <a  class="right bold">Edit</a>
                            </div>
                            <p class="bold">@if(!empty(Auth::user()->fullname)){{Auth::user()->fullname}}@else{{"No Name"}}@endif</p>
                            <a >{{Auth::user()->email}}</a><br />
                            <a href="{{url('')}}" >Change Password</a>
                        </div>
                        <div class="big_small_sec right news">
                        	<div class="headng">
                                <h6 class="bold">Newsletters</h6>
                                <a  class="right bold">Edit</a>
                            </div>
                            <p class="bold">Jhon</p>
                            <a >john@website.com</a><br />
                            <a >Change Password</a>
                        </div>
                        <div class="clear"></div>
                        <div class="botm_big">&nbsp;</div>
                    </div>
                    <div class="clear"></div>
                    <div class="big_sec left">
                        <div class="big_small_sec left">
                        	<h6 class="bold">Default Billing Address</h6>
                            <p>You have not set a default billing address.</p>
                            <a ><u>Edit Address</u></a>
                        </div>
                        <div class="big_small_sec right news">
                        	<h6 class="bold">Default Shipping Address</h6>
                            <p>You have not set a default shipping address.</p>
                            <a ><u>Edit Address</u></a>
                        </div>
                        <div class="clear"></div>
                        <div class="botm_big">&nbsp;</div>
                    </div>
                </div>
                <h6 class="heading bold colr">Recent Orders</h6>
                <div class="account_table">
                	<ul class="headtable">
                    	<li class="order bold">Order</li>
                        <li class="date bold">Date</li>
                        <li class="ship bold">Ship to</li>
                        <li class="ordertotal bold">Order Total</li>
                        <li class="status bold last">Status</li>
                        <li class="action bold last">&nbsp;</li>
                    </ul>
                    @if(!empty($detail))
                    @foreach($detail as $item)                    
                    <ul class="contable">
                    	<li class="order">{{$item->quantity}}</li>
                        <li class="date">16/10/2010</li>
                        <li class="ship">{{Auth::user()->name}}</li>
                        <li class="ordertotal">{{$item->price}}</li>
                        <li class="status last">Shiped</li>
                        <li class="action last"><a  class="first" href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->product->name)])!!}">View</a><a >Edit</a></li>
                    </ul>    
                    @endforeach
                    @endif                
                </div>
                <div class="view_tags">
                	<div class="viewssec">
                    	<h4 class="heading colr">Recent Views</h4>
                    	<ul>
                        	<li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="ratingsblt">
                                	<a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_grey.gif" alt="" /></a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="ratingsblt">
                                	<a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_grey.gif" alt="" /></a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="ratingsblt">
                                	<a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_grey.gif" alt="" /></a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="ratingsblt">
                                	<a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_green.gif" alt="" /></a>
                                    <a ><img src="images/star_grey.gif" alt="" /></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="tagssec">
                    	<h4 class="heading colr">My Recent Tags</h4>
                    	<ul>
                        	<li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="tgs">
                                	<p class="colr tag">Tags: </p>
                                    <a >Leatehr Bags, </a>
                                    <a >Bags, </a>
                                    <a >Laptop Bags</a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="tgs">
                                	<p class="colr tag">Tags: </p>
                                    <a >Leatehr Bags, </a>
                                    <a >Bags, </a>
                                    <a >Laptop Bags</a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="tgs">
                                	<p class="colr tag">Tags: </p>
                                    <a >Leatehr Bags, </a>
                                    <a >Bags, </a>
                                    <a >Laptop Bags</a>
                                </div>
                            </li>
                            <li>
                            	<h5 class="bullet">1</h5>
                                <h5 class="title">Product Name</h5>
                                <div class="clear"></div>
                                <div class="tgs">
                                	<p class="colr tag">Tags: </p>
                                    <a >Leatehr Bags, </a>
                                    <a >Bags, </a>
                                    <a >Laptop Bags</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
            <div class="clear"></div>
            <div class="col2_botm">&nbsp;</div>
        </div>
        <!-- Column1 Section -->
    	<div class="col1">
        	<div class="myaccount">
            	<div class="small_heading">
                	<h5>My Account</h5>
                </div>
                <ul>
                	<li><a  class="selected">Account Dashboard</a></li>
                    <li><a >Account Information</a></li>
                    <li><a >Address Book</a></li>
                    <li><a >My Orders</a></li>
                    <li><a >My Product Reviews</a></li>
                    <li><a >My Tags</a></li>
                    <li><a >My Wishlist</a></li>
                    <li><a >Newsletter Subscriptions</a></li>
                    <li><a >My Downloadable Products</a></li>
                </ul>
                <div class="clear"></div>
                <div class="left_botm">&nbsp;</div>
            </div>
            <!-- Categories -->
                <div class="category">
                	<div class="col1center">
                    <div class="small_heading">
                        <h5>Categories</h5>
                    </div>
                    <div class="glossymenu">
                        <a class="menuitem submenuheader"  >Sports Coverage</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader"  >Le Vele Bedding</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader"  >LHF Bedding</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader"  >Pacific Coast</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader"  >SnugFleece Woolens</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>
                        <a class="menuitem submenuheader"  >Southern Textiles</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="listing.html">Le Vele Bedding</a></li>
                                <li><a href="listing.html">LHF Bedding</a></li>
                                <li><a href="listing.html">Pacific Coast</a></li>
                                <li><a href="listing.html">SnugFleece Woolens</a></li>
                                <li><a href="listing.html">Southern Textiles</a></li>
                            </ul>
                        </div>	
                    </div>
                    </div>
                    <div class="clear"></div>
                    <div class="left_botm">&nbsp;</div>
                </div>
                <!-- My Cart Products -->
                <div class="mycart">
                	<div class="col1center">
                    <div class="small_heading">
                        <h5>My Cart</h5>
                        <div class="clear"></div>
                        <span class="veiwitems">(3) Items - <a href="cart.html" class="colr">View Cart</a></span>
                    </div>
                    <ul>
                        <li>
                            <p class="bold title">
                                <a href="detail.html">Armani Tweed Blazer</a>
                            </p>
                            <div class="grey">
                                <p class="left">QTY: <span class="bold">3</span></p>
                                <p class="right">Price: <span class="bold">$200</span></p>
                            </div>
                        </li>
                        <li>
                            <p class="bold title">
                                <a href="detail.html">Armani Tweed Blazer</a>
                            </p>
                            <div class="grey">
                                <p class="left">QTY: <span class="bold">3</span></p>
                                <p class="right">Price: <span class="bold">$200</span></p>
                            </div>
                        </li>
                        <li>
                            <p class="bold title">
                                <a href="detail.html">Armani Tweed Blazer</a>
                            </p>
                            <div class="grey">
                                <p class="left">QTY: <span class="bold">3</span></p>
                                <p class="right">Price: <span class="bold">$200</span></p>
                            </div>
                        </li>
                    </ul>
                    <p class="right bold sub">Sub total: $600</p>
                    <div class="clear"></div>
                    <a  class="simplebtn right"><span>Checkout</span></a>
                    </div>
                    <div class="clear"></div>
                    <div class="left_botm">&nbsp;</div>
                </div>
                <div class="poll">
                <div class="col1center">
            	<div class="small_heading">
            		<h5>Poll</h5>
                </div>
                <p>What is your favorite Magento feature?</p>
                <ul>
                	<li><input name="layerd" type="radio" value="" /> Layered Navigation</li>
                    <li><input name="price" type="radio" value="" /> Price Rules</li>
                    <li><input name="category" type="radio" value="" /> Category Management</li>
                    <li><input name="compare" type="radio" value="" /> Compare Products</li>
                </ul>
                <a  class="simplebtn"><span>Vote</span></a>
                </div>
                <div class="clear"></div>
                    <div class="left_botm">&nbsp;</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>
@endsection