{{App::setLocale(session('lang'))}}
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="csrf_token" content="{{ csrf_token() }}" />
        <title>Online Shop</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="@yield('description')"/>
        <meta name="author" content=""/>        
        <link rel="stylesheet" id="skin-link" href="{!!asset('public/Estore16/css/style.css')!!}" type="text/css" />
        <link rel="stylesheet" href="{!!asset('public/Estore16/css/nivo-slider.css')!!}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{!!asset('public/Estore16/css/default.advanced.css')!!}" type="text/css" />
        <link rel="stylesheet" href="{!!asset('public/Estore16/css/contentslider.css')!!}" type="text/css"  />
        <link rel="stylesheet" href="{!!asset('public/Estore16/css/jquery.fancybox-1.3.1.css')!!}" type="text/css" media="screen" />  
        <link href="{!!asset('public/js/jqueryui/jquery-ui.css')!!}" rel="stylesheet">
        <link rel="shortcut icon" href="{!! asset('public/sanpham/Apple-03.png')!!}">          
        <link href="{{ asset("public/css/flags.css") }}" rel="stylesheet">
        <base href="http://localhost/elishop/">
        <script type="text/javascript"  src="{!! asset('public/Estore16/js/skins.js')!!}"></script>

    </head>
    <body id="body">
        <a name="top"></a>
        <div id="wrapper_sec">       
            @widget('header')                 
            <div class="clear"></div> 
            @yield('banner')
            @yield('crumb')       
            <div class="content_sec">
                @yield('content')            
            </div>
            <div class="clear"></div>
        </div>

        @include('Estore16.footer')
        <script type="text/javascript" src="{!! asset('public/Estore16/js/jquery.js')!!}"></script>
        
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jquery.pjax.js')!!}"></script>
        <script>
            $(document).ready(function(){
                
                $.get('getward/' + $('#country').val(), function(data) {
                        $("#city").html(data);
                    });
                $('#country').change(function(){
                    $.get('getward/' + $(this).val(), function(data) {
                        $("#city").html(data);
                    }); 
                });
                $(".colorpick-btn").click(function () {
                    createCookie("current-skin", $(this).attr('rel'), 10);
                    createCookie("current-adv", $(this).attr('name'), 10);                    
                    window.location.reload(true);
                });
            })
        </script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jquery.min14.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jquery.easing.1.2.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jcarousellite_1.0.1.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/scroll.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/ddaccordion.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/acordn.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/cufon-yui.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/Trebuchet_MS_400-Trebuchet_MS_700-Trebuchet_MS_italic_700-Trebuchet_MS_italic_400.font.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/cufon.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/contentslider.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jquery.fancybox-1.3.1.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/lightbox.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/jquery.nivo.slider.pack.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/nivo.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('public/Estore16/js/myscript.js')!!}"></script>

    <!-- Include library's JS files -->
    
    <script src="{!!asset('public/js/jqueryui/jquery-ui.js')!!}"></script>
    
    <script>
      $(function() {     
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',showAnim: 'drop', changeMonth: true,
          changeYear: true,yearRange: '1990:2015'});
    });
  </script>
  <script type="text/javascript">
    $(function(){
        $('.addr').click(function(){
            $('.estimate').toggle(1000);
        });
        $("div.alert").delay(5000).slideUp();
    });
</script>

<script type="text/javascript">
        var timer;

    function up()
    {
        timer = setTimeout(function()
        {
            var keywords = $('#searchBox').val();
            var token = $('meta[name="csrf_token"]').attr('content');
            
            if (keywords.length >= 0)
            {

                $.ajax({
                    url: 'executeSearch',
                    data: {"keywords": keywords,"token": token}, 
                    success: function(response){
                        $('#product-search').html(response);
                       // here you can grab the response which would probably be 
                       // the extra fields you want to generate and display it
                    }
                });
            }
        }, 500);
    }

    function down()
    {
        clearTimeout(timer);
    }    
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function xacnhanxoa(msg){
        if (window.confirm(msg)){
            return true;
        }
        return false;
    }
            // $(function(){
            // // pjax
            //     $(document).pjax('a', '#body')
            // })
            // $(document).ready(function(){
            // // does current browser support PJAX
            //     if ($.support.pjax) {
            //         $.pjax.defaults.timeout = 2000; // time in milliseconds
            //     }
            // });
</script>
        
    </body>
</html>