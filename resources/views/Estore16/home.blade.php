@extends('Estore16.master')
@section('description','Tri\'s blog')

<!-- Scroolling Products -->
@section('content')
	@if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @elseif(Session::has('danger'))
        <div class="alert alert-danger">
            {{ Session::get('danger') }}
        </div>
    @endif

    @include('Estore16.featured')
    <!-- Column1 Section -->   
    @widget('cate') 
@endsection
