@extends('Estore16.master')

@section('crumb')
<div class="crumb">
    <ul>
        <li class="first"><a href="#">Home</a></li>
        <li>Cart</li>
    </ul>
</div>
@endsection
@section('content')
<div class="col3">
    @if($content->count()>0)
    <div class="col3_top">&nbsp;</div>
    <div class="col3_center" id="product-search"></div>
    <div class="col3_center">
        <h2 class="heading colr">BedSheets</h2>
        <div class="shoppingcart">
         <form method="POST" action="{!!url('update')!!}">
            <input type="hidden" name="_token" value="{!! csrf_token()!!}" />
            <table class="table table-striped table-bordered">
                <tr>
                    <th class="image">Image</th>
                    <th class="name">Product Name</th>
                    <th class="quantity">Qty</th>
                    <th class="total">Action</th>
                    <th class="price">Unit Price</th>
                    <th class="total">Total</th>

                </tr>
               
                    @foreach($content as $item)
                    <tr>
                        <td class="image"><a href="#"><img title="product" alt="product" src="{!!asset('public/sanpham/'.$item['options']['img'])!!}" height="50" width="50"></a></td>
                        <td  class="name"><a href="#">{!!$item["name"]!!}</a></td>           
                        <td class="quantity"><input title="number must be larger than 0" onkeypress="return isNumber(event)" class="qty span1" type="text" size="1" value="{!!$item['qty']!!}" name="quantity[{!!$item['rowid']!!}]">             
                        </td>
                        <td class="total"> <a href="javascript:void(0)" class="updatecart" id="{!!$item['rowid']!!}"><img class="tooltip-test" data-original-title="Update" src="{!! asset('public/Estore16/images/update.png')!!}" alt=""></a>
                            <a onclick="return xacnhanxoa('Are u sure?');" href="{!!url('xoa-sp',[$item['rowid']])!!}"><img class="tooltip-test" data-original-title="Remove"  src="{!! asset('public/Estore16/images/remove.png')!!}" alt=""></a></td>                        
                        <td class="price">{!!number_format($item['price'],0,'.',',')!!}</td>
                        <td class="total">{!!number_format($item['price']*$item['qty'],0,'.',',')!!}</td>

                    </tr>

                    @endforeach
                
            </table>

            <div class="clear"></div>
            <div class="subtotal">
                <a href="{!!url('/')!!}" class="simplebtn"><span>Continue Shopping</span></a>                
                <a class="simplebtn" href="javascript:void(0)" onclick="$(this).closest('form').submit();"><span>Update</span></a>
                <a href="{!!url('xoa-het')!!}" class="simplebtn"><span>Delete all</span></a>
                <h3 class="colr">{!!number_format($total,0,'.',',')!!}</h3>
            </div>
            </form>
            <div class="clear"></div>
            <div class="sections">
                <div class="cartitems">
                    <h6 class="colr">Based on your selection, you may be interested in the following items:</h6>
                    <ul>
                        <li>
                            <div class="thumb">
                                <a href="detail.html"><img src="public/Estore16/images/prod_cart.gif" alt="" /></a>
                            </div>
                            <div class="desc">

                                <a href="#" class="title bold">Alexander Christie</a>
                                <p class="bold">$300</p>
                                <a href="#" class="simplebtn"><span>Add to Cart</span></a>
                                <div class="clear"></div><br />
                                <a href="#"><u>Add to Wishlist</u></a><br />
                                <a href="#"><u>Add to Compare</u></a>
                            </div>
                        </li>
                        <li>
                            <div class="thumb">
                                <a href="detail.html"><img src="public/Estore16/images/prod_cart.gif" alt="" /></a>
                            </div>
                            <div class="desc">
                                <a href="detail.html" class="title bold">Alexander Christie</a>
                                <p class="bold">$300</p>
                                <a href="cart.html" class="simplebtn"><span>Add to Cart</span></a>
                                <div class="clear"></div><br />
                                <a href="#"><u>Add to Wishlist</u></a><br />
                                <a href="#"><u>Add to Compare</u></a>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                    <div class="sec_botm">&nbsp;</div>
                </div>
                <div class="centersec">
                <form method="POST" action="{!!url('check-out')!!}">
                    <input type="hidden" name="_token" value="{!! csrf_token()!!}" />
                    <div class="discount">
                        <h6 class="colr">Discount Codes</h6>
                        <p>Enter your coupon code if you have one.</p>
                        <ul>
                            <li><input name="discount" type="text" class="bar" /></li>
                            <li><a href="#" class="simplebtn"><span>Apply Coupon</span></a></li>
                        </ul>
                        <div class="clear"></div>
                        <div class="sec_botm">&nbsp;</div>
                    </div>
                    <a class="simplebtn addr" href="javascript:void(0)"><span>Or you have another addr</span></a>
                    
                    <div class="estimate" style="display:none">
                        <h6 class="colr">Estimate Shipping and Tax</h6>
                        <p>Enter your destination to get a shipping estimate.</p>
                        
                        <ul>
                            <li class="bold">Receiver name</li>
                            <li><input name="txtName" type="text" class="bar" /></li>                            
                        </ul>
                        <ul>
                            <li class="bold">Receiver Phone</li>
                            <li><input name="txtPhone" type="text" class="bar" /></li>                            
                        </ul>
                        <ul>
                            <li class="bold">Receiver addr</li>
                            <li><input name="txtAddr" type="text" class="bar" /></li>                            
                        </ul>
                        <ul>
                            <li class="bold">Country</li>
                            <li>
                                <select id="country" name="city" class="selectbox">
                                    <option value="0">Plz select ur city...</option>
                                    @foreach($cities as $item)
                                        <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                                    @endforeach
                                </select>
                            </li>
                        </ul>
                        <ul>
                            <li class="bold">State/Province</li>
                            <li>
                                <select name="district" id="city">                                
                                </select>
                            </li>
                        </ul>
                        <ul>
                            <li class="bold">Zip code</li>
                            <li><input name="discount" type="text" class="bar" /></li>
                            <li><button type="submit" class="simplebtn" value="Submit">Submit</button></li>
                        </ul>
                        
                        <div class="clear"></div>
                        <div class="sec_botm">&nbsp;</div>
                    </div>
                    </div>
                    <div class="grand_total">
                        <ul>
                            <li class="title">Subtotal</li>
                            <li class="price bold">{!!number_format($total,0,'.',',')!!}</li>
                        </ul>
                        <ul>
                            <li class="title"><h5>Grand total</h5></li>
                            <li class="price"><h5>{!!number_format($total/100000,0,'.',',')!!}mil</h5></li>
                        </ul>
                        <div class="clear"></div>
                        <button type="submit" class="proceed right" value="Proceed to Checkout">Proceed to Checkout</button>
                        <div class="clear"></div>
                        <a href="#" class="right">Checkout with Multiple Addresses</a>
                        <div class="clear"></div>
                        <div class="sec_botm">&nbsp;</div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="col3_botm">&nbsp;</div>
    @else
    <div class="col3_center">
        <div class="shoppingcart">
            <br>
            <h2>No any product is selected, yet!!!</h2>
            <div class="subtotal">
                <a href="{!!url()!!}" class="simplebtn"><span>Home</span></a>
            </div>
            <br>
        </div>
    </div>
    @endif
</div>
@endsection