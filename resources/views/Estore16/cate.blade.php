@extends('Estore16.master')
@section('description',"$cate->name")

@endsection
@section('crumb')
<div class="crumb">
    <ul>
        <li class="first"><a href="{!!url()!!}">Home</a></li>
        <li>{!!$cate->name!!}</li>
    </ul>
</div>
@endsection
@section('content')  
<div class="col2">
    <div class="col2_top">&nbsp;</div>     
    <div class="col3_center" id="product-search"></div>
    <div class="col3_center">
        <div class="sorting">
            <p class="left colr">{!!$product_cate->total()!!} Item(s)</p>
            <div class="right">                    
                <ul class="paging">
                    @if($product_cate->currentPage()>3)
                    <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()-5))!!}@if($sort!='')&amp;sort={{$sort}}@endif">Prev</a></li>
                    @endif
                    @for($i=1;$i<=$product_cate->lastPage();$i++)
                    @if($i>$product_cate->currentPage()-5 && $i<$product_cate->currentPage()+5)
                    <li class="{!!($product_cate->currentPage()==$i) ? 'active':''!!}"><a href="{!!str_replace('/?','?',$product_cate->url($i))!!}@if($sort!='')&amp;sort={{$sort}}@endif">{!!$i!!}</a></li>                      
                    @endif
                    @endfor
                    @if($product_cate->currentPage()<$product_cate->lastPage()-2)
                    <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()+5))!!}@if($sort!='')&amp;sort={{$sort}}@endif">Next</a></li>
                    @endif
                </ul>                      

            </div>
            <div class="clear"></div>
            <p class="left">View as: <a href="javascript:void(0)" id="grid" class="bold">Grid</a>&nbsp;<a href="javascript:void(0)" id="list" class="colr">List</a></p>
            <ul class="right">
                <li class="text">
                    Sort by Position
                    <a href="{!!url('loai-san-pham',[$cate->id,changeTitle(strtolower($cate->name))])!!}?sort=@if($sort=='name'){{'-name'}}@else{{'name'}}@endif" class="@if($sort=='name'||$sort=='-name'){{'bold'}}@else {{'colr'}}@endif">Name </a>
                    <a href="{!!url('loai-san-pham',[$cate->id,changeTitle(strtolower($cate->name))])!!}?sort=@if($sort=='price'){{'-price'}}@else{{'price'}}@endif" class="@if($sort=='price'||$sort=='-price'){{'bold'}}@else {{'colr'}}@endif">Price</a> 
                </li>
            </ul>
        </div>
        <div class="listing">
            <h4 class="heading colr">New Products for March 2010</h4>
            <ul>
                @if(count($product_cate)>0)
                @foreach($product_cate as $key => $item)
                <li {!!($key+1)%4==0? "class='last four'":""!!}>
                    <a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}" class="thumb"><img src="{!!asset('public/sanpham/'.$item->pimages[0]->image)!!}" onerror="this.src = '{!!asset("public/sanpham/noImage.jpg")!!}'" alt="" /></a>
                    <h6 class="colr">{!!$item->name!!}</h6>
                    <div class="stars">
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">{!!$item->view!!} Reviews</a>
                    </div>
                    <div class="addwish">
                        <a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>
                    <div class="cart_price">
                        <a href="{!!url('mua-hang',[$item->id,changeTitle($item->name)])!!}" class="adcart">Add to Cart</a>
                        <p class="price">{!!number_format($item->price,0,'.',',')!!}</p>
                    </div>  
                    
                </li>  
                <div class="display inactive">{{$item->description}}</div>
                @endforeach
                @else
                <h2>Products are gonna be updated...</h2>
                @endif
            </ul>
        </div>
        <div class="sorting">            
            <div class="right">                    
                <ul class="paging">
                    @if($product_cate->currentPage()>3)
                    <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()-5))!!}@if($sort!='')&amp;sort={{$sort}}@endif">Prev</a></li>
                    @endif
                    @for($i=1;$i<=$product_cate->lastPage();$i++)
                    @if($i>$product_cate->currentPage()-5 && $i<$product_cate->currentPage()+5)
                    <li class="{!!($product_cate->currentPage()==$i) ? 'active':''!!}"><a href="{!!str_replace('/?','?',$product_cate->url($i))!!}@if($sort!='')&amp;sort={{$sort}}@endif">{!!$i!!}</a></li>                      
                    @endif
                    @endfor
                    @if($product_cate->currentPage()<$product_cate->lastPage()-2)
                    <li><a href="{!!str_replace('/?','?',$product_cate->url($product_cate->currentPage()+5))!!}@if($sort!='')&amp;sort={{$sort}}@endif">Next</a></li>
                    @endif
                </ul>                      

            </div>
        </div>
    </div>
</div>
@widget('cate') 
@endsection
