@extends('Estore16.master')
@section('description')
{!!$product_detail[0]->name!!}
@endsection
@section('crumb')
    <div class="crumb">
        <ul>
            <li class="first"><a href="{!!url()!!}">Home</a></li>
            <li><a href="{!!url('loai-san-pham',[$product_detail[0]->cate->id,changeTitle($product_detail[0]->cate->name)])!!}">{!!$product_detail[0]->cate->name!!}</a></li>
            <li>{!!$product_detail[0]->name!!}</li>
        </ul>
    </div>
@endsection
@section('content')     
    <div class="col2">
    	<div class="col2_top">&nbsp;</div>
        <div class="col3_center" id="product-search"></div>
        <div class="col2_center">
    	<h4 class="heading colr">Canyon Crest "Maestro" Comforter Set</h4>
        <div class="prod_detail">
        	<div class="big_thumb">
            	<div id="slider2">
                @foreach($product_detail[0]->pimages as $images)
                    <div class="contentdiv">
                    	<img src="{!!asset('public/sanpham/'.$images->image)!!}" onerror="this.src = '{!!asset("public/sanpham/noImage.jpg")!!}'" alt="" />
                        <a rel="example_group" href="{!!asset('public/sanpham/'.$images->image)!!}" onerror="this.href = '{!!asset("public/sanpham/noImage.jpg")!!}'" title="Lorem ipsum dolor sit amet, consectetuer adipiscing elit." class="zoom">&nbsp;</a>
                    </div>
                  @endforeach
                
                </div>
                <a href="javascript:void(null)" class="prevsmall"><img src="{!!asset('public/Estore16/images/prev.gif')!!}" alt="" /></a>
                <div style="float:left; width:189px !important; overflow:hidden;">
                <div class="anyClass" id="paginate-slider2">
                    <ul>
                    @foreach($product_detail[0]->pimages as $images)
                        <li><a href="javascript:void(0)" class="toc"><img src="{!!asset('public/sanpham/'.$images->image)!!}" onerror="this.src = '{!!asset("public/sanpham/noImage.jpg")!!}'" alt="" /></a></li>
                    @endforeach
                    </ul>
                </div>
                </div>
                <a href="javascript:void(null)" class="nextsmall"><img src="{!!asset('public/Estore16/images/next.gif')!!}" alt="" /></a>
                <script type="text/javascript" src="js/cont_slidr.js"></script>
            </div>
            <div class="desc">
            	<div class="quickreview">
                        @if($product_detail[0]->view==0)
                        <a href="#" class="bold black left"><u>Be the first to review this product</u></a>
                        @else
                        <u>{!!$product_detail[0]->view!!} review(s)</u>
                        @endif
                        <div class="clear"></div>
                        <p class="avail"><span class="bold">Availability:</span> In stock</p>
                      <h6 class="black">Quick Overview</h6>
                    <p>
                    	{!!$product_detail[0]->description!!}
                    </p>
                </div>
                <div class="addtocart">
                	<h4 class="left price colr bold">{!!number_format($product_detail[0]->price,0,'.',',')!!}</h4>
                        <div class="clear"></div>
                        <ul class="margn addicons">
                            <li>
                                <a href="#">Add to Wishlist</a>
                            </li>
                            <li>
                                <a href="#">Add to Compare</a>
                            </li>
                    	</ul>
                        <div class="clear"></div>
                    <ul class="left qt">                                   	                           
                        <li><a href="{!!url('mua-hang',[$product_detail[0]->id,changeTitle($product_detail[0]->name)])!!}" class="simplebtn"><span>Add To Cart</span></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <div class="prod_desc">
            	<h4 class="heading colr">Product Description</h4>
                <p>
                	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed elit. Nulla sem risus, vestibulum in, volutpat eget, dapibus ac, lectus. Curabitur dolor sapien, hendrerit non, suscipit bibendum, auctor ac, arcu. Vestibulum dapibus. Sed pede lacus, pretium in, condimentum sit amet, mollis dapibus, magna. Ut bibendum dolor nec augue. Ut tempus luctus metus. Sed a velit. Pellentesque at libero elementum ante condimentum sollicitudin. Pellentesque lorem ipsum, semper quis.  interdum et, sollicitudin eu, purus. Vivamus fringilla ipsum vel orci. Phasellus vitae massa at massa pulvinar pellentesque. Fusce tincidunt libero vitae odio. Donec malesuada diam nec mi. Integer hendrerit pulvinar ante. Donec eleifend, nisl eget aliquam congue, justo metus venenatis neque, vel tincidunt elit augue sit amet velit. Nulla facilisi. Aenean suscipit. 
                </p>
            </div>
        </div>
        <div class="listing">
        	<h4 class="heading colr">New Products for March 2010</h4>
            <ul>
                <li>
                	<a href="detail.html" class="thumb"><img src="{!!asset('public/Estore16/images/prod4.gif')!!}" alt="" /></a>
                    <h6 class="colr">Armani Tweed Blazer</h6>
                    <div class="stars">
                    	<a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">(3) Reviews</a>
                    </div>
                    <div class="addwish">
                    	<a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>
                    <div class="cart_price">
                    	<a href="cart.html" class="adcart">Add to Cart</a>
                        <p class="price">$399.99</p>
                    </div>
                </li>
                <li>
                	<a href="detail.html" class="thumb"><img src="{!!asset('public/Estore16/images/prod4.gif')!!}" alt="" /></a>
                    <h6 class="colr">Armani Tweed Blazer</h6>
                    <div class="stars">
                    	<a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">(3) Reviews</a>
                    </div>
                    <div class="addwish">
                    	<a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>
                    <div class="cart_price">
                    	<a href="cart.html" class="adcart">Add to Cart</a>
                        <p class="price">$399.99</p>
                    </div>
                </li>
                <li>
                	<a href="detail.html" class="thumb"><img src="{!!asset('public/Estore16/images/prod4.gif')!!}" alt="" /></a>
                    <h6 class="colr">Armani Tweed Blazer</h6>
                    <div class="stars">
                    	<a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">(3) Reviews</a>
                    </div>
                    <div class="addwish">
                    	<a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>

                    <div class="cart_price">
                    	<a href="cart.html" class="adcart">Add to Cart</a>
                        <p class="price">$399.99</p>
                    </div>
                </li>
                <li class="last">
                	<a href="detail.html" class="thumb"><img src="{!!asset('public/Estore16/images/prod4.gif')!!}" alt="" /></a>
                    <h6 class="colr">Armani Tweed Blazer</h6>
                    <div class="stars">
                    	<a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">(3) Reviews</a>
                    </div>
                    <div class="addwish">
                    	<a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>
                    <div class="cart_price">
                    	<a href="cart.html" class="adcart">Add to Cart</a>
                        <p class="price">$399.99</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="tags_big">
        	<h4 class="heading">Product Tags</h4>
            <p>Add Your Tags:</p>
            <span><input name="tags" type="text" class="bar" /></span>
            <div class="clear"></div>
            <span><a href="#" class="simplebtn"><span>Add Tags</span></a></span>
            <p>Use spaces to separate tags. Use single quotes (') for phrases.</p>
        </div>
        <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="col2_botm">&nbsp;</div>              
    </div>
    
@widget('cate') 
@endsection
