<div class="listing">            
            <ul>
                @if(count($searchProduct)>0)
                <p>{{count($searchProduct)}} item(s)</p>
                @foreach($searchProduct as $key => $item)
                <li {!!($key+1)%4==0? "class='last four'":""!!}>
                    <a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}" class="thumb"><img src="{!!asset('public/sanpham/'.$item->pimages[0]->image)!!}" onerror="this.src = '{!!asset("public/sanpham/noImage.jpg")!!}'" alt="" /></a>
                    <h6 class="colr">{!!$item->name!!}</h6>
                    <div class="stars">
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                        <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                        <a href="#">{!!$item->view!!} Reviews</a>
                    </div>
                    <div class="addwish">
                        <a href="#">Add to Wishlist</a>
                        <a href="#">Add to Compare</a>
                    </div>
                    <div class="cart_price">
                        <a href="{!!url('mua-hang',[$item->id,changeTitle($item->name)])!!}" class="adcart">Add to Cart</a>
                        <p class="price">{!!number_format($item->price,0,'.',',')!!}</p>
                    </div>  
                    
                </li>  
                <div class="display inactive">{{$item->description}}</div>
                @endforeach
                @else
                <h2 class="not-found" style="color:grey; font-size:30px;">Not found...</h2>                
                @endif
            </ul>
</div>

   