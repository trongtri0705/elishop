<div class="col1">
    <!-- Categories -->
    <div class="category">
        <div class="col1center">
            <div class="small_heading">
                <h5>Categories</h5>
            </div>
            <div class="glossymenu">
                @foreach($cate as $parent)
                <a class="menuitem submenuheader" href="" >{!!$parent->name!!}</a>    
                <div class="submenu">
                    @if($parent->cate->count())
                            <ul>                             
                            @foreach($parent->cate as $child)
                            @if($child->active)
                                <li><a href="{!!url('loai-san-pham',[$child->id,changeTitle($child->name)])!!}">{!!$child->name!!}</a></li>                                
                            @endif
                            @endforeach  
                            </ul>
                    @endif
                        </div>
                @endforeach
            </div>
        </div>
        <div class="clear"></div>
        <div class="left_botm">&nbsp;</div>
    </div>
    <!-- My Cart Products -->
    <div class="mycart">
        <div class="col1center">
            <div class="small_heading">
                <h5>My Cart</h5>
                <div class="clear"></div>
                <span class="veiwitems">{!!Cart::count()!!} - <a href="{!!url('gio-hang')!!}" class="colr">View Cart</a></span>
            </div>
            <ul>
                @foreach(Cart::content() as $item)
                <li>
                    <p class="bold title">
                        <a href="detail.html">{!!$item['name']!!}</a>
                    </p>
                    <div class="grey">
                        <p class="left">QTY: <span class="bold">{!!$item['qty']!!}</span></p>
                        <p class="right">Price: <span class="bold">{!!number_format($item['price'],0,'.',',')!!}</span></p>
                    </div>
                </li>
                @endforeach                
            </ul>
            <p class="right bold sub">Sub total: {!!Cart::total()!!}</p>
            <div class="clear"></div>
            @if(Cart::total()>0)
            <a href="checkout" class="simplebtn right"><span>Checkout</span></a>
            @endif
        </div>
        <div class="clear"></div>
        <div class="left_botm">&nbsp;</div>
    </div>
    <div class="poll">
        <div class="col1center">
            <div class="small_heading">
                <h5>Poll</h5>
            </div>
            <p>What is your favorite Magento feature?</p>
            <ul>
                <li><input name="layerd" type="radio" value="" /> Layered Navigation</li>
                <li><input name="price" type="radio" value="" /> Price Rules</li>
                <li><input name="category" type="radio" value="" /> Category Management</li>
                <li><input name="compare" type="radio" value="" /> Compare Products</li>
            </ul>
            <a href="#" class="simplebtn"><span>Vote</span></a>
        </div>
        <div class="clear"></div>
        <div class="left_botm">&nbsp;</div>
    </div>
    <div class="clear"></div>
</div>
