<!-- Column2 Section -->

    <div class="col2">
        <div class="col2_top">&nbsp;</div>
        <div class="col2_center" style="min-height:0" id="product-search"></div>
        <div class="col2_center">
            <h4 class="heading colr">Featured Products</h4>
            <div id="prod_scroller">
                <a href="javascript:void(null)" class="prev">&nbsp;</a>
                <div class="anyClass scrol">
                    <ul>
                    @foreach($featured as $item)
                        <li>
                            <a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}"><img src="{!!asset('public/sanpham/'.getImage($item->id))!!}" alt="" /></a>
                            <h6 class="colr">{!!$item->name!!}</h6>
                            <p class="price bold">{!!number_format($item->price,0,'.',',')!!}</p>
                            <a href="{!!url('mua-hang',[$item->id,changeTitle($item->name)])!!}" class="adcart">Add to Cart</a>
                        </li> 
                    @endforeach                       
                    </ul>
                </div>
                <a href="javascript:void(null)" class="next">&nbsp;</a>
            </div>
            <div class="clear"></div>
            <div class="listing">
                <h4 class="heading colr">New Products for March 2010</h4>
                <ul id="product-search">
                @foreach($latest as $key => $item)
                    <li {!!($key+1)%4==0? "class='last'":""!!}>
                        <a href="{!!url('chi-tiet-san-pham',[$item->id,changeTitle($item->name)])!!}" class="thumb"><img src="{!!asset('public/sanpham/'.getImage($item->id))!!}" alt="" /></a>
                        <h6 class="colr">Armani Tweed Blazer</h6>
                        <div class="stars">
                            <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                            <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                            <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                            <a href="#"><img src="{!!asset('public/Estore16/images/star_green.gif')!!}" alt="" /></a>
                            <a href="#"><img src="{!!asset('public/Estore16/images/star_grey.gif')!!}" alt="" /></a>
                            <a href="#">({!!$item->view!!}) Reviews</a>
                        </div>
                        <div class="addwish">
                            <a href="#">Add to Wishlist</a>
                            <a href="#">Add to Compare</a>
                        </div>
                        <div class="cart_price">
                            <a href="{!!url('mua-hang',[$item->id,changeTitle($item->name)])!!}" class="adcart">Add to Cart</a>
                            <p class="price">{!!number_format($item->price,0,'.',',')!!}</p>
                        </div>
                    </li>
                @endforeach                    
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <div class="col2_botm">&nbsp;</div>
    </div>