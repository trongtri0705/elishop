@extends('Estore16.master')
@section('content')

<div class="content_sec">
    <div class="col3">
        <div class="col3_top">&nbsp;</div>
        <div class="col3_center">
            <h2 class="heading colr">Check Out</h2>
            <div class="login">
                <div class="registrd">
                 <h3>Step 1 - Delivery</h3>               
                 <form class="form-horizontal" role="form" method="POST" action="{!!url('authentication/postuser')!!}">
                    <input type="hidden" name="_token" value="{!! csrf_token()!!}" />
                    @include('admin.block.error') 
                    <h2>{{Session::get('msg')}}</h2>
                    <ul class="forms">
                        <li class="txt">Email Address <span class="req">*</span></li>
                        <li class="inputfield"><input class="form-control" value="{!! old('txtUser',isset($data)? $data['email'] :null)!!}" name="txtUser" type="email" disabled></li>
                    </ul>
                    <ul class="forms">
                        <li class="txt">Full name </li>
                        <li class="inputfield"><input class="form-control" placeholder="Full name" name="txtFullname" type="text" value="{!! old('txtFullname',isset($data)? $data['fullname'] :null)!!}" autofocus></li>
                    </ul>
                    <ul class="forms">
                        <li class="txt">Phone</li>
                        <li class="inputfield"><input class="form-control" placeholder="Phone" name="txtPhone" type="text" value="{!! old('txtPhone',isset($data)? $data['phone'] :'')!!}"></li>                        
                    </ul>
                    <ul class="forms">
                        <li class="txt">Birth</li>
                        <li class="inputfield"><input type="text" id="datepicker" name="txtBirth" size="20" value="{!! old('txtBirth',isset($data)? $data['birth'] :null)!!}"></li>                        
                    </ul>
                    <ul class="forms">
                        <li class="txt">Address</li>
                        <select id="country" name="city" class="selectbox">
                            <option value="0">Please Select a country</option>
                            @foreach($cities as $item)
                            <option value="{!!$item->id!!}" @if($item->id==Auth::user()->id_city){{'selected'}}@endif>{!!$item->name!!}</option>
                            @endforeach
                        </select>
                    </ul>
                    <ul class="forms">
                        <label>Ward</label>
                        <select name="district" id="city">                                
                        </select>
                    </ul>
                    <ul class="forms">
                        <li class="txt">&nbsp;</li>
                        <li><button class="simplebtn" type="submit" class="btn btn-lg btn-success btn-block">Edit</button></li>
                    </ul>
                </form>                        
            </div>            
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>


@stop