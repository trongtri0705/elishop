@extends('Estore16.master')
@section('content')
  <div class="content_sec">
        <div class="col3">
            <div class="col3_top">&nbsp;</div>
            <div class="col3_center">
                <h2 class="heading colr">Register</h2>
                <div class="login">
                    <div class="registrd">                       
                    @include('admin.block.error') 
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('authentication/postRegister') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <ul class="forms">
                            <li class="txt">Email Address <span class="req">*</span></li>
                            <li class="inputfield"><input type="email" class="form-control" name="txtUser" value="{{ old('email') }}"></li>
                        </ul>
                        <ul class="forms">
                            <li class="txt">Password <span class="req">*</span></li>
                            <li class="inputfield"><input type="password" class="form-control" name="txtPass"></li>
                        </ul>
                        <ul class="forms">
                            <li class="txt">Confirm Password <span class="req">*</span></li>
                            <li class="inputfield"><input type="password" class="form-control" name="txtRePass"></li>
                        </ul>
                        <ul class="forms">
                            <li class="txt"></li>
                            <li class="inputfield"><button class="simplebtn" type="submit" class="btn btn-lg btn-success btn-block">Register</button></li>
                        </ul>                        
                    </form>
                 </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="col3_botm">&nbsp;</div>
        </div>
    </div>
    <div class="clear"></div>
@endsection