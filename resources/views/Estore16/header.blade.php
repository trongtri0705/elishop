<div class="secnd_navi">
    <ul class="links">
        <li>Default welcome msg!</li>
        <li><a href="#">{{trans('routes.mywl')}}</a></li>
        <li><a href="@if(Auth::check()){!!url('account')!!}@endif">{{trans('routes.myac')}}</a></li>
        <li><a href="{!!url('gio-hang')!!}">{{trans('routes.myct')}}</a></li>
        
        
        @if (Auth::check())
            <li><a href="{{ url('auth/logout') }}">{{trans('routes.lo')}}</a></li>
            <li><a href="{{ url('authentication/getuser') }}">@if(Auth::user()->fullname == "") {!!Auth::user()->email!!} @else {!!Auth::user()->fullname!!} @endif</a></li>
            
        @else
            <li><a href="{{ url('auth/login') }}">{{trans('routes.lg')}}</a></li>
            <li><a href="{{ url('authentication/getRegister') }}">{{trans('routes.rg')}}</a></li>
        @endif
        @foreach (Config::get('app.locales') as $locale => $language)
            <li class='<?= $locale == App::getLocale() ? 'current' : '' ?>'>
                <a href="@if($locale != App::getLocale()){!!url('language',[$locale])!!}@else{!!'javascript:void(0)'!!}@endif"><img src="blank.gif" class="flag flag-{{$locale}}" alt="" /></a>
            </li>
        @endforeach     
    </ul>
    <ul>
            
        </ul>
    <ul class="network">
        <li>Share with us:</li>
        <li><a href="#"><img src="{!!asset('public/Estore16/images/linkdin.gif')!!}" alt="" /></a></li>
        <li><a href="#"><img src="{!!asset('public/Estore16/images/rss.gif')!!}" alt="" /></a></li>
        <li><a href="#"><img src="{!!asset('public/Estore16/images/twitter.gif')!!}" alt="" /></a></li>
        <li><a href="#"><img src="{!!asset('public/Estore16/images/facebook.gif')!!}" alt="" /></a></li>
    </ul>
   
        
   
</div>
<div class="clear"></div>
<div class="logo">
    <a href="{!!url()!!}"><img src="{!!asset('public/Estore16/images/logo.png')!!}" alt="" /></a>
    <h5 class="slogn">The best watches for all</h5>
</div>

<ul class="search">
    <li>
        <form id="peditform" method="POST" action="executeSearch"> 
        <input data-token="{{ csrf_token() }}" onkeydown="down()" onkeyup="up()" type="text" value="Search" id="searchBox" name="_token" onblur="if (this.value == '') { this.value = 'Search'; }" onfocus="if (this.value == 'Search') { this.value = ''; }" class="bar" />
        </form>
    </li>   
</ul>

<div class="clear"></div>
<div class="navigation">
    <ul id="nav" class="dropdown dropdown-linear dropdown-columnar">        
        @foreach($cate as $category)
        @if($category->has('cate'))
        <li class="dir"><a href="javascript:void(0)">{!!$category->name!!}</a>

            @if($category->cate->count())
            <ul class="bordergr small">
                
                <li class="dir">
                <span class="colr navihead bold">{!!$category->name!!}</span>

                <ul>
                @foreach($category->cate as $subcategory)
                    @if($subcategory->active)
                    <li class="clear"><a href="{!!url('loai-san-pham',[$subcategory->id,changeTitle(strtolower($subcategory->name))])!!}">{!!$subcategory->name!!} </a></li>
                    @endif
                @endforeach  
                </ul>
                </li>
                
            </ul>
            @endif
        </li>
        @else
        <li class="dir"><a href="javascript:void(0)">{!!$category->name!!}</a>
            @endif

            @endforeach              
            <li class="dir"><a href="#">{{trans('routes.theme')}}</a>
                    <ul class="bordergr small">
                        <li class="dir"><span class="colr navihead bold">Themes</span>
                            <ul>
                                <li class="clear"><a class="colorpick-btn" href="{!!url()!!}" style="background-color:#2dc3e8;" rel="public/Estore16/css/style.css" name="public/Estore16/css/default.advanced.css">Blue</a></li>
                                <li class="clear"><a href="../green/index.html">Green</a></li>
                                <li class="clear"><a href="../orange/index.html">Orange</a></li>
                                <li class="clear"><a class="colorpick-btn" href="{!!url()!!}" style="background-color:#8C0095;" rel="public/Estore16/purple/css/style.css" name="public/Estore16/purple/css/default.advanced.css">Purple</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

        </ul>
    </div>   

