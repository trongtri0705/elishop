@extends('layouts.master')

@section('content')

    <h1>Department</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Order</th><th>Active</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $department->id }}</td> <td> {{ $department->name }} </td><td> {{ $department->order }} </td><td> {{ $department->active }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection