<?php

return array(
    'contact' => 'Lien he',
    'about'   => 'gioi thieu',
    'myac'	  => 'Tài khoản của tôi',
    'mywl'	  => 'Sản phẩm quan tâm',
    'myct'	  => 'Giỏ hàng',
    'ck'	  => 'Thanh toán',
    'lo'	  => 'Đăng xuất',
    'lg'  	  => 'Đăng nhập',
    'rg'	  => 'Đăng kí',
    'theme'	  => 'Giao diện',
);