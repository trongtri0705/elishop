<?php

// app/lang/en/routes.php

return array(
    'contact' => 'contact',
    'about'   => 'about-us',
    'myac'	  => 'My Acccount',
    'mywl'	  => 'My Wishlist',
    'myct'	  => 'My Cart',
    'ck'	  => 'Checkout',
    'lo'	  => 'Logout',
    'lg'  	  => 'Login',
    'rg'	  => 'Register',
    'theme'	  => 'Themes',
);