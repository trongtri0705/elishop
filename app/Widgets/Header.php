<?php

namespace App\Widgets;
use App\Product;
use App\Department;
use App\Category;
use Arrilot\Widgets\AbstractWidget;

class Header extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $cate = Department::with(['cate'])->where('active',1)->get();
        return view("Estore16.header", [
            'config' => $this->config,
            'cate' => $cate,
        ]);
    }
}