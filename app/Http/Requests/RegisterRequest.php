<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'email'=>'required|unique:eli_shop,eli_user',
            'password'=>'required',
            'password_confirmation'=>"same:password",
        ];
    }
    public function messages()
    {
        return[
        'email.required'=>'Nhap email chua ma doi login',
        'email.unique'=>'Co nguoi xai roi m oi',
        'password.required'=>'M ko co pass ma doi login ah?'
        'password_confirmation.same'=>'Sao pw ko trung khop?'
        ];
    }
}
