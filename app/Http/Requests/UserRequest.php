<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'txtUser' => 'required|unique:eli_user,email|email',
            'txtPass' => 'required',
            'txtRePass' => 'required|same:txtPass',
        ];
    }    
    public function messages()
    {
        return[
            'txtUser.required' => 'Nhap name di ba',
            'txtUser.unique' => 'Trung roi ba',
            'txtPass.required' => 'nhap pass vo ba',
            'txtRePass.required' => 'Nhap lai pass kia ba',
            'txtRePass.same' => 'hack ha ong noi',            
        ];
    }
}
