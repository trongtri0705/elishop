<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CateRequest;
use App\Cate;
class CateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        //
        $data = Cate::findOrFail($id)->toArray();
        $parent = Cate::where('id','<>',$id)->get()->toArray();
        return view('admin.cate.edit',compact('parent','data','id'));
    }
    public function postEdit(Request $request, $id){
        
        $this->validate($request,
            ["txtCateName"=>"required"],
            ["txtCateName.required"=>"Please enter cate's name."]);
        $cate=Cate::find($id);
         $cate->name = $request->txtCateName;
        $cate->alias = changeTitle($request->txtAlias);
        if($cate->alias==""){$cate->alias=changeTitle(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $cate->name));}
        $cate->order = $request->txtOrder;
        // $cate->parent_id = $request->txtParent;
        $cate->parent_id = $request->sltParent;
        $cate->keywords = $request->txtKeywords;
        $cate->description = $request->txtDescription;
        $cate->save();
        return redirect()->route('admin.cate.list')->with('success','Edited completely!');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $parent = Cate::where('parent_id',$id)->count();
        if($parent==0){
            $cate = Cate::find($id);
            $cate->delete($id);
            return redirect()->route('admin.cate.list')->with('success','successfully deleted');
        }
       else{
        echo "<script type='text/javascript'>
            alert('Its denied!');
            window.location='"; echo route('admin.cate.list'); 
        echo "'
        </script>";
       }
    }
    // public function getDelete($id){
        
    // }
    public function getList(){
        $data = Cate::select('id','name','parent_id')->orderBy('id','desc')->get()->toArray();
        return view('admin.cate.list',compact('data'));
    }
    public function getAdd(){
        $parent = Cate::select('id','name','parent_id')->get()->toArray();
        return view('admin.cate.add',compact('parent'));
    }
    public function postAdd(CateRequest $request){
        $cate = new Cate();
        $cate->name = $request->txtCateName;
        $cate->alias = changeTitle($request->txtAlias);
        if($cate->alias==""){$cate->alias=changeTitle(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $cate->name));}
        $cate->order = $request->txtOrder;
        // $cate->parent_id = $request->txtParent;
        $cate->parent_id = $request->sltParent;
        $cate->keywords = $request->txtKeywords;
        $cate->description = $request->txtDescription;
        $cate->save();
        return redirect()->route('admin.cate.list')->with('success','Posted completely!');
    }
}
