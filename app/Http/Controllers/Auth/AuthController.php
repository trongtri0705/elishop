<?php namespace App\Http\Controllers\Auth;
use Illuminate\Http\RedirectResponse;
use App\User,App\City;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Hash;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Validator;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\ThrottlesLogins;
class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers,ThrottlesLogins;
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */	
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		if (\Auth::check())
		{
		    return redirect()->intended('/');
		}
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
		$this->middleware('guest', ['except' => 'getLogout']);		
	}	
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	 
	public function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}
	// public function getLoginAdmin()
	// {
	// 	 if (view()->exists('auth.authenticate')) {
 //           return view('auth.authenticate');
 //       }
	// 	return view('admin.login');
	// }
	// public function postLoginAdmin(LoginRequest $request)
	// {
	// 	$login = array(
	// 		'email' => $request->email,
	// 		'password'=>$request->password,
	// 		'id_level'=>1,
	// 	);
	// 	if($this->auth->attempt($login)){
	// 		return new RedirectResponse(url('/home'));
	// 	}
	// 	else
	// 		return redirect()->back()->withErrors(['errors', 'Login fail']);
	// }
	// public function getLogin()
	// {
	// 	if (\Auth::check())
	// 	{
	// 	    return redirect()->intended('/home');
	// 	}	
	// 	return view('Estore16.login');
	// }

	// public function postLogin(LoginRequest $request)
	// {
	// 	$login = array(
	// 		'email' => $request->email,
	// 		'password'=>$request->password,
	// 	);	
	// 	if($this->auth->attempt($login)){
	// 		return redirect('/');
	// 	}	
	// 	else
	// 		return redirect()->back()->with(['msg'=>'Login fail']);
	// }

	public function getRegister()
	{
		return view('Estore16.register');
	}

	public function postRegister(UserRequest $request)
	{
		$user = new User;		
		$user->email = $request->txtUser;
		$user->password = Hash::make($request->txtPass);				
		$user->save();
		return redirect()->intended()->with('success','gioi ta, ban biet dang ky roi');
	}
	public function getUser()
	{	
		$cities = City::get();
		$id = Auth::user()->id;
		$data = User::find($id);       
        return view('Estore16.account',compact('data','id','cities'));
	}

	public function postUser(Request $request){	

		$id = Auth::user()->id;	
        $user = User::find($id);        
        $user->fullname = $request->txtFullname;
 		$user->phone = $request->txtPhone;
 		$user->birth = $request->txtBirth;
 		$user->id_district = $request->district;
 		$user->id_city = $request->city;
 		$user->address = $request->address;
        $user->save();
        return redirect()->intended('home');
    }
	// public function getLogout()
	// {
	// 	auth()->logout();
	// 	return redirect('home');
	// }
	public function getEdit(){        
        return view('Estore16.repass');
    }
    public function postEdit(Request $request){
        $user = User::where('email',$request->txtUser)->first();
        // var_dump($user);
        // exit();
        if(empty($user)){
        	// var_dump($user);
        	// exit();
        	return redirect()->back()->where('success','No results...');
        }
        else if($request->input('txtPass')){
            
            $this->validate($request,
            ["txtRePass"=>"same:txtPass"],
            ["txtRePass.same"=>"Password isn't match"]);
            $pass = $request->input('txtPass');
            $user->password = Hash::make($pass);

        }  
        $user->remember_token = $request->input('_token');              
        $user->save();
        return redirect()->intended()->with('success','Edited completely!');;

    }

}
