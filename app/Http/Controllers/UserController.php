<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Http\Requests\UserRequest;
use Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user_cur = Auth::user()->id;
        $user = User::find($id);
        if(($id == 1)||($id == $user_cur)||($user_cur!=1 && $user['lever']==1)){
            return redirect()->route('admin.user.list')->with('danger','alert!!! not allowed');
        }
        else{
        $user->delete($id);
        return redirect()->route('admin.user.list')->with('success','successfully deleted');
        }
    }
   
    public function getAdd()
    {
        return view('admin/user/add');
    }
    public function postAdd(UserRequest $request)
    {
        $user = new User();
        $user->username = $request->txtUser;
        $user->password = Hash::make($request->txtPass);        
        $user->email = $request->txtEmail;
        $user->level = $request->rdoLevel;
        $user->save();
        return redirect()->route('admin.user.list')->with('success','Created User completely!');
    }
    public function getList()
    {
        $data = User::select('*')->orderBy('id','desc')->get()->toArray();
        return view('admin.user.list',compact('data'));
    }
    public function getEdit($id){
        $data = User::find($id);
        if((Auth::user()->id !=1)&& ($id == 1 ||($data["level"]==1 && Auth::user()->id != $id))){
            return redirect()->route('admin.user.list')->with('danger','Sorry! You r not allowed to modify this user');;
        }
        return view('admin.user.edit',compact('data','id'));
    }
    public function postEdit($id,Request $request){
        $user = User::find($id);
        if($request->input('txtPass')){
            
            $this->validate($request,
            ["txtRePass"=>"same:txtPass"],
            ["txtRePass.same"=>"Password isn't match"]);
            $pass = $request->input('txtPass');
            $user->password = Hash::make($pass);

        }
        $user->level = $request->rdoLevel;
        $user->email = $request->txtEmail;
        $user->remember_token = $request->input('_token');
        $user->save();
        return redirect()->route('admin.user.list')->with('success','Edited completely!');;

    }
}
