<?php namespace App\Http\Controllers;
use DB,Auth;
use Illuminate\Support\Str;
use Mail,Cart;
use App\Product;
use App\Order,App\OrderDetail;
use App\Ward,App\City;
use App\Department;
use App\Category;
use App\User;
use Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Hash;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public function index()
	{
		$latest = DB::table('eli_product')
			->select('*')
			->where('active',1)
			->orderBy('id','desc')
			->limit(8)
			->get();
		$featured = DB::table('eli_product')
			->select('*')
			->where('active',1)
			->limit(10)
			->get();		
		return view('Estore16.home',compact('featured','latest'));
	}
	public function locale()
	{
		return view('locale');
	}
	public function getCate(){
		
		return view('Estore16/header',compact('parent','child'));
	}
	public function loaisanpham($id){
		if(!empty(Request::get('sort'))){
			$sort = Request::get('sort');
			if($sort[0]!='-')
				$product_cate = Product::with('pimages')->where(['id_category'=>$id,'active'=>1])->orderBy($sort)->paginate(20);
			else
				$product_cate = Product::with('pimages')->where(['id_category'=>$id,'active'=>1])->orderBy(substr($sort, 1),'desc')->paginate(20);		
		}
		else{
			$sort='';
			$product_cate = Product::with('pimages')->where(['id_category'=>$id,'active'=>1])->paginate(20);

		}
		$cate = DB::table('eli_category')
			->select('*')
			->where('id',$id)
			->first();
		$bestseller = DB::table('eli_product')
			->select('*')
			->limit(10)
			->get();
		$menu_cate = DB::table('eli_category')
			->select('*')
			->where('id_department',$cate->id_department)
			->get();
		$latest = DB::table('eli_product')
			->select('*')
			->orderBy('id','desc')
			->limit(10)
			->get();
		return view('Estore16.cate',compact('cate','product_cate','menu_cate','latest','bestseller','sort'));
	}
	public function chitietsanpham($id){
		$product_detail = Product::where('id',$id)->with(['pimages','cate'])->get();		
		$product_cate = Category::where('id',$product_detail[0]->id_category)->get();
		// var_dump($product_cate[0]['name']);
		// exit();
		$product_detail[0]->view++;
		DB::table('eli_product')
            ->where('id', $id)
            ->update(array('view' => $product_detail[0]->view));
		return view('Estore16.detail',compact('product_detail','product_cate'));
	}
	public function getLienhe(){
		return view('Estore16.contact');
	}
	public function postLienhe(Request $request){
		$data = [
				'hoten'   => Request::input('name'),
				'tinnhan' => Request::input('message')
				];
		Mail::send('emails.blanks',$data,function($message){
			$message->from('trongtri0705@gmail.com','Nguyen Trong Tri');
			$message->to('ng.trongtri.00@gmail.com','Christ')
					->subject('This is mail');
		});
		echo "<script>
			alert('Tks');
			window.location = '";
			echo url('/');
			echo "';
		</script>
		";
	}
	public function muahang($id){
		$product_buy = Product::where('id',$id)->get();		
		Cart::add(array('id'=>$id,'name'=>$product_buy[0]->name,'qty'=>1,'price'=>$product_buy[0]->price,'options'=>array('img'=>$product_buy[0]->pimages[0]->image)));
		$content = Cart::content();
		return redirect()->route('giohang');
	}
	public function giohang(){
		$cities = City::with('ward')->get();
		$content = Cart::content();
		$total = Cart::total();		
		return view('Estore16.giohang',compact('content','total','cities'));
	}
	public function xoasp($id){
		Cart::remove($id);
		return redirect()->route('giohang');
	}
	public function xoahet(){
		Cart::destroy();
		return redirect()->route('giohang');
	}
	public function capnhat($id){
		if(Request::ajax()){
			$id = Request::get('id');
			$qty = max(request::get('qty'),1);
			Cart::update($id,$qty);
			echo "oke";
		}
	} 
	public function update(){
		$cart = Request::input('quantity');
		foreach($cart as $key => $item)
		{
			Cart::update($key,$item);
		}
		return redirect()->route('giohang');
	} 
	public function getIndex()
	{
		return view('home.index');
	}	
	public function logout()
	{
		Auth::logout();
		return redirect()->route('/');
	}
	public function checkout(){	
		if(!Auth::check())
			return redirect('authentication/getlogin');
		else{

			$content = Cart::content();
			$total = Cart::total();
			$order = new Order;
			$order->id_user = Auth::user()->id;
			$order->email = Auth::user()->email;
			$order->total = $total;
			if(empty(Request::Input('txtName')))
			{
				$order->receiver_name = Auth::user()->fullname;
				$order->receiver_phone = Auth::user()->phone;
				$order->receiver_address = Auth::user()->address;
				$order->id_district = Auth::user()->id_district;
				$order->id_city = Auth::user()->id_city;
				
			}else{
				$order->receiver_name = Request::Input('txtName');
				$order->receiver_phone = Request::Input('txtPhone');
				$order->receiver_address = Request::Input('txtAddr');
				$order->id_district = Request::Input('district');
				$order->id_city = Request::Input('city');
			}
			// if($order->save())
			// 	print_r("success");
			// else
			// 	print_r("uns");
			// exit();
			$order->save();
			foreach ($content as $key => $value) {
				# code...
				$ordtail = new OrderDetail;
				$ordtail->id_order = $order->id;
				$ordtail->id_product = $value->id;
				$ordtail->quantity = $value->qty;
				$ordtail->price = $value->price; 
				$ordtail->save();
			}
			$data = [
				'cart'   => Cart::content(),				
				];
			Mail::send('emails.blanks',$data,function($message){
				$message->from('trongtri0705@gmail.com','Nguyen Trong Tri');
				$message->to(Auth::user()->email,Auth::user()->name)
						->subject('Don dat hang');
			});
			Cart::destroy();

			echo "<script>
				alert('Tks');
				window.location = '";
				echo url('/');
				echo "';
			</script>
		";
			// return redirect()->intended('home')->with('success','completely!');
		}		
	}
	public function getWard($id)
	{
		$ward = Ward::where('id_city',$id)->orderBy('name')->get();
		return view('ward',compact('ward'));
	}
	public function executeSearch()
    {
    	if(Request::ajax()){
	        $keywords = Request::get('keywords');

	        // $searchProduct = Product::where('name','like','%'.$keywords.'%')->paginate(20);
	        $users = Product::all();
	        $searchProduct=new \Illuminate\Database\Eloquent\Collection();

	        foreach($users as $u)
	        {
	            if(Str::contains(Str::lower($u->name), Str::lower($keywords)))
	                $searchProduct->add($u);
	        }
	        return view('Estore16.searchProduct',compact('searchProduct'));
	    }
    }
    public function account(){
    	$cart = Order::where('id_user',Auth::user()->id)->first();
    	
    	$detail = OrderDetail::with('product')->where('id_order',$cart->id)->get();
    	
    	return view('Estore16.detailaccount',compact('detail'));
    }
}
