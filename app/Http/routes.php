<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','WelcomeController@index');
Route::get('/home', 'WelcomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	]);
// ,'middleware'=>'auth'
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	Route::group(['prefix'=>'cate'],function(){
		Route::get('list',['as'=>'admin.cate.list','uses'=>'CateController@getList']);
		Route::get('add',['as'=>'admin.cate.getAdd','uses'=>'CateController@getAdd']);
		Route::post('add',['as'=>'admin.cate.postAdd','uses'=>'CateController@postAdd']);
		Route::get('destroy/{id}',['as'=>'admin.cate.destroy','uses'=>'CateController@destroy']);
		Route::get('edit/{id}',['as'=>'admin.cate.getEdit','uses'=>'CateController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.cate.postEdit','uses'=>'CateController@postEdit']);
	});
	Route::group(['prefix'=>'product'],function(){
		Route::get('list',['as'=>'admin.product.list','uses'=>'ProductController@getList']);
		Route::get('add',['as'=>'admin.product.getAdd','uses'=>'ProductController@getAdd']);
		Route::post('add',['as'=>'admin.product.postAdd','uses'=>'ProductController@postAdd']);
		Route::get('destroy/{id}',['as'=>'admin.product.destroy','uses'=>'ProductController@destroy']);
		Route::get('edit/{id}',['as'=>'admin.product.getEdit','uses'=>'ProductController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.product.postEdit','uses'=>'ProductController@postEdit']);
		Route::get('delimg/{id}',['as'=>'admin.product.getDelimg','uses'=>'ProductController@getDelimg']);
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('list',['as'=>'admin.user.list','uses'=>'UserController@getList']);
		Route::get('add',['as'=>'admin.user.getAdd','uses'=>'UserController@getAdd']);
		Route::post('add',['as'=>'admin.user.postAdd','uses'=>'UserController@postAdd']);
		Route::get('destroy/{id}',['as'=>'admin.user.destroy','uses'=>'UserController@destroy']);
		Route::get('edit/{id}',['as'=>'admin.user.getEdit','uses'=>'UserController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.user.postEdit','uses'=>'UserController@postEdit']);
	});
});
Route::resource('product', 'ProductController');
Route::group(array('middleware' => 'auth'), function(){
	Route::controller('filemanager', 'FilemanagerLaravelController');
});
Route::resource('user', 'UserController');
Route::get('test',function(){
	return view('simpleone.home');
});
Route::get('contact',function(){
	return view('simpleone.contact');
});
Route::get('language/{lan?}', function ($lan='en'){	
	Session::put('lang', $lan);
	return redirect()->back();
});
Route::get('auth/getedit', 'Auth\AuthController@getEdit');
Route::post('auth/postedit', 'Auth\AuthController@postEdit');
Route::get('auth/getlogin', 'Auth\AuthController@getLogin');
Route::get('authentication/getRegister', 'Auth\AuthController@getRegister');
Route::post('authentication/postRegister', 'Auth\AuthController@postRegister');
Route::post('auth/postlogin', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getlogout');
Route::get('authentication/getuser', 'Auth\AuthController@getUser');
Route::post('authentication/postuser', 'Auth\AuthController@postUser');
Route::get('loai-san-pham/{id}/{tenloai}',['as'=>'loaisanpham','uses'=>'WelcomeController@loaisanpham']);
Route::get('chi-tiet-san-pham/{id}/{tenloai}',['as'=>'chitietsanpham','uses'=>'WelcomeController@chitietsanpham']);
Route::get('lien-he',['as'=>'getLienhe','uses'=>'WelcomeController@getLienhe']);
Route::post('lien-he',['as'=>'postLienhe','uses'=>'WelcomeController@postLienhe']);
Route::get('mua-hang/{id}/{tensanpham}',['as'=>'muahang','uses'=>'WelcomeController@muahang']);
Route::get('gio-hang',['as'=>'giohang','uses'=>'WelcomeController@giohang']);
Route::get('xoa-sp/{id}',['as'=>'xoasp','uses'=>'WelcomeController@xoasp']);
Route::get('xoa-het','WelcomeController@xoahet');
Route::post('update','WelcomeController@update');
Route::get('cap-nhat/{id}/{qty}',['as'=>'capnhat','uses'=>'WelcomeController@capnhat']);
Route::post('check-out',['as'=>'checkout','uses'=>'WelcomeController@checkout']);
Route::resource('department', 'DepartmentController');
Route::get('getward/{id}','WelcomeController@getWard');
Route::get('account','WelcomeController@account');
Route::filter('localization', function() {
	App::setLocale(Auth::user()->locale);
});
Route::get('executeSearch', 'WelcomeController@executeSearch');
// Route::get('/{locale}',function(){
// 	// print_r($locale);
// 	// exit();
// 	if( App::setLocale(Session::get('locale')))
// 		print_r('ok');
// 	else print_r('no');
// 	exit();
// 	return redirect('home');
// });