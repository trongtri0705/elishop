<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

	//
	protected $table = 'eli_city';
	protected $fillable = ['name'];
	public $timestamps = false;
	public function ward(){
		return $this->hasMany(Ward::class,'id_city');
	}
}
