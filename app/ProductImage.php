<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {

	//
	protected $table = 'eli_product_image';
	protected $fillable = ['image','id_product'];
	public $timestamps = false;
	public function product(){
		return $this->belongsTo('App\Product');
	}
}
