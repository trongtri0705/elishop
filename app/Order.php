<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

	//
	protected $table = 'eli_order';
	protected $fillable = ['id_user','email','receiver_name','receiver_phone','receiver_address','id_district','id_city','total'];
	public $timestamps = false;
	public function detail(){
		return $this->hasMany('App\OrderDetail','id_order');
	}
	public function user()
	{
		return $this->belongsTo('App\User','id_user');
	}
}
