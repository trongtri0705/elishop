<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model {

	//
	protected $table = 'eli_order_detail';
	protected $fillable = ['id_order','id_product','quantity','price'];
	public $timestamps = false;
	public function product(){
		return $this->belongsTo('App\Product','id_product');
	}	
}
