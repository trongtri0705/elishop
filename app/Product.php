<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	//
	protected $table = 'eli_product';
	protected $fillable = ['id_category','name','order','price','promotion','quantity','content','view','description','active'];
	public $timestamps = false;
	public function cate(){
		return $this->belongsTo('App\Category','id_category');
	}
	public function pimages(){
		return $this->hasMany(ProductImage::class,'id_product');
	}
}
