<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	//
	protected $table = 'eli_category';
	protected $fillable = ['id_department','name','order'];
	public $timestamps = false;
	public function department(){
		return $this->belongsTo('App\Department','id_department');
	}
	public function product(){
		return $this->hasMany(Product::class,'id_category');
	}
}
