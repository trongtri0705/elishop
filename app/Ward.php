<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model {

	//
	protected $table = 'eli_district';
	protected $fillable = ['id_city','name'];
	public $timestamps = false;
	public function city(){
		return $this->belongsTo('App\City','id_city');
	}
}
