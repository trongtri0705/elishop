<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'eli_department';
    public $timestamps = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'order', 'active'];
    public function cate(){
        return $this->hasMany(Category::class,'id_department');
    }

}
