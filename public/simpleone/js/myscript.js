$(document).ready(function(){
	$('.updatecart').click(function(){
		var rowid = $(this).attr('id');
		// alert(rowid);
		var href = window.location.href ;
		var qty = $(this).parent().parent().find('.qty').val();
		var token = $("input[name='_token']").val();
		$.ajax({
			url:'cap-nhat/'+rowid+'/'+qty,
			type:'GET',
			cache:false,
			data:{'_token':token,'id':rowid,'qty':qty},
			success:function(data){
				if(data=="oke"){
					// alert("success");
					window.location = href
				}
			}
		});		

	});
	$("nav li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	$('#type-view').click(function(){
		// $('.span3').toggleClass('list-view');
		
		if($(this).html()=="LIST"){
			$('.thumbnails.grid>.span3').addClass('span9');
			$('.thumbnails.grid>.span9').removeClass('span3');
			$(this).text("GRID");
			$('.display').removeClass('inactive');
			$('.display').addClass('active');
		}
		else{$(this).text("LIST");
			$('.thumbnails.grid>.span9').addClass('span3');
			$('.thumbnails.grid>.span3').removeClass('span9');
			$('.display').removeClass('active');
			$('.display').addClass('inactive');
		}
	});
});