var timer;

function up()
{
    timer = setTimeout(function()
    {
        var keywords = $('#searchBox').val();
        var token = $('input').data('token');
        if (keywords.length > 0)
        {

            $.ajax({
                url:'executeSearch',
                type:'POST',
                cache:false,
                data:{token:'_token',keywords:'keywords'},
                success:function(data){
                    $('#search-results').html(markup);
                }
            }); 
        }
    }, 500);
}

function down()
{
    clearTimeout(timer);
}