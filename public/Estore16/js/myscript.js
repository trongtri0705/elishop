$(document).ready(function(){
	$('.updatecart').click(function(){
		var rowid = $(this).attr('id');
		// alert(rowid);
		var href = window.location.href ;
		var qty = $(this).parent().parent().find('.qty').val();
		var token = $("input[name='_token']").val();
		$.ajax({
			url:'cap-nhat/'+rowid+'/'+qty,
			type:'GET',
			cache:false,
			data:{'_token':token,'id':rowid,'qty':qty},
			success:function(data){
				if(data=="oke"){
					// alert("success");
					window.location = href
				}
			}
		});		

	});
	$('.item_list').attr('style', '');
	$('#list').click(function(){
		// $('.span3').toggleClass('list-view');
		$(this).removeClass('colr');
		$(this).addClass('bold');
		$('.listing li').addClass('list');
		$('#grid').removeClass('bold');
		$('#grid').addClass('colr');
		$('.display').removeClass('inactive');
		$('.display').addClass('active');
		$('.four').removeClass('last');
	});
	$('#grid').click(function(){
		// $('.span3').toggleClass('list-view');
		$(this).removeClass('colr');
		$(this).addClass('bold');
		$('#list').removeClass('bold');
		$('#list').addClass('colr');
		$('.listing li').removeClass('list');
		$('.display').removeClass('active');
		$('.display').addClass('inactive');
		$('.four').addClass('last');
	});
});
